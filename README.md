Spatial interpolation / prediction using Ensemble Machine Learning
================
Created and maintained by: Tom Hengl
(<a href="mailto:tom.hengl@OpenGeoHub.org" class="email">tom.hengl@OpenGeoHub.org</a>)
\|
Last compiled on: 23 November, 2020



-   [![alt text](tex/R_logo.svg.png "About")
    Introduction](#alt-text-introduction)
    -   [Ensemble Machine Learning](#ensemble-machine-learning)
    -   [Using geographical distances to improve spatial
        interpolation](#using-geographical-distances-to-improve-spatial-interpolation)
    -   [EML using the landmap package](#eml-using-the-landmap-package)
-   [![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction
    of Zinc meuse data
    set](#alt-text-example-spatial-prediction-of-zinc-meuse-data-set)
    -   [Interpolation of numeric values using spatial
        regression](#interpolation-of-numeric-values-using-spatial-regression)
    -   [Model fine-tuning and feature
        selection](#model-fine-tuning-and-feature-selection)
    -   [Estimation of prediction
        intervals](#estimation-of-prediction-intervals)
    -   [Predictions using log-transformed target
        variable](#predictions-using-log-transformed-target-variable)
-   [![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction
    of soil types
    (factor-variable)](#alt-text-example-spatial-prediction-of-soil-types-factor-variable)
    -   [EML for classification](#eml-for-classification)
    -   [Classification accuracy](#classification-accuracy)
-   [![alt text](tex/R_logo.svg.png "Multi-scale") Example: Spatial
    prediction of soil organic carbon using 2-scale
    model](#alt-text-example-spatial-prediction-of-soil-organic-carbon-using-2-scale-model)
    -   [Coarse-scale model](#coarse-scale-model)
    -   [Fine-scale model](#fine-scale-model)
    -   [Merge multi-scale predictions](#merge-multi-scale-predictions)
-   [Summary notes](#summary-notes)
-   [References](#references)

[<img src="tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International
License](http://creativecommons.org/licenses/by-sa/4.0/).

![alt text](tex/R_logo.svg.png "About") Introduction
----------------------------------------------------

#### Ensemble Machine Learning

Ensembles are predictive models that combine predictions from two or
more learners (Seni & Elder, [2010](#ref-seni2010ensemble); Zhang & Ma,
[2012](#ref-zhang2012ensemble)). The specific benefits of using Ensemble
learners are:

-   **Performance**: they can help improve the average prediction
    performance over any individual contributing learner in the
    ensemble.
-   **Robustness**: they can help reduce extrapolation / overshooting
    effects of individual learners.
-   **Unbiasness**: they can help determine a model-free estimate of
    prediction errors.

Even the most flexible and best performing learners such as Random
Forest or neural networks always carry a bias in the sense that the
fitting produces recognizable patterns and these are limited by the
properties of the algorithm. In the case of ensembles, the modeling
algorithm becomes secondary, and even though the improvements in
accuracy are often minor as compared to the best individual learner,
there is a good chance that the final EML model will be less prone to
overshooting and extrapolation problems.

There are in principle three ways to apply ensembles (Zhang & Ma,
[2012](#ref-zhang2012ensemble)):

-   *bagging*: learn in parallel, then combine using some deterministic
    principle (e.g. weighted averaging),
-   *boosting*: learn sequentially in an adaptive way, then combine
    using some deterministic principle,
-   *stacking*: learn in parallel, then fit a meta-model to predict
    ensemble estimates,

The “meta-model” is an additional model that basically combines all
individual or “base learners”. In this tutorial we focus only on the
stacking approach to EML.

There are several packages in R that implement Ensemble Machine
Learning, for example:

-   [SuperLearner](https://cran.r-project.org/web/packages/SuperLearner/vignettes/Guide-to-SuperLearner.html)
    package,
-   [caretEnsemble](https://cran.r-project.org/web/packages/caretEnsemble/vignettes/caretEnsemble-intro.html)
    package,
-   [h2o.stackedEnsemble](http://docs.h2o.ai/h2o-tutorials/latest-stable/tutorials/ensembles-stacking/index.html)
    package,
-   [mlr](https://mlr.mlr-org.com/reference/makeStackedLearner.html) and
    [mlr3](https://mlr3gallery.mlr-org.com/posts/2020-04-27-tuning-stacking/)
    packages,

Ensemble Machine Learning is also available in Python through the
[scikit-learn](https://scikit-learn.org/stable/modules/ensemble.html)
library.

In this tutorial we focus primarily on using the [mlr
package](https://mlr.mlr-org.com/), i.e. a wrapper functions to mlr
implemented in the landmap package.

#### Using geographical distances to improve spatial interpolation

Machine Learning was for long time been considered suboptimal for
spatial interpolation problems, in comparison to classifical
geostatistical techniques such as kriging, because it basically ignores
spatial dependence structure in the data. To incorporate spatial
dependence structures in machine learning, one can now add the so-called
“geographical features”: buffer distance, oblique distances, and/or
distances in the watershed, as features. This has shown to improve
prediction performance and produce maps that visually appear as they
have been produced by kriging (Hengl, Nussbaum, Wright, Heuvelink, &
Gräler, [2018](#ref-hengl2018random)).

Use of geographical as features in machine learning for spatial
predictions is explained in detail in:

-   Behrens, T., Schmidt, K., Viscarra Rossel, R. A., Gries, P.,
    Scholten, T., & MacMillan, R. A. (2018). [Spatial modelling with
    Euclidean distance fields and machine
    learning](https://doi.org/10.1111/ejss.12687). European journal of
    soil science, 69(5), 757-770.
-   Hengl, T., Nussbaum, M., Wright, M. N., Heuvelink, G. B., &
    Gräler, B. (2018). [Random forest as a generic framework for
    predictive modeling of spatial and spatio-temporal
    variables](https://doi.org/10.7717/peerj.5518). PeerJ, 6, e5518.
    <a href="https://doi.org/10.7717/peerj.5518" class="uri">https://doi.org/10.7717/peerj.5518</a>
-   Møller, A. B., Beucher, A. M., Pouladi, N., and Greve, M. H. (2020).
    [Oblique geographic coordinates as covariates for digital soil
    mapping](https://doi.org/10.5194/soil-6-269-2020). SOIL, 6, 269–289,
    <a href="https://doi.org/10.5194/soil-6-269-2020" class="uri">https://doi.org/10.5194/soil-6-269-2020</a>
-   Sekulić, A., Kilibarda, M., Heuvelink, G.B., Nikolić, M., Bajat, B.
    (2020). [Random Forest Spatial
    Interpolation](https://doi.org/10.3390/rs12101687). Remote Sens.
    12, 1687.
    <a href="https://doi.org/10.3390/rs12101687" class="uri">https://doi.org/10.3390/rs12101687</a>

In the case the number of covariates / features becomes large, and
assuming the covariates are diverse, and that the points are equally
spread in an area of interest, there is probably no need for using
geographical distances in model training because unique combinations of
features become so large that they can be used to represent
*geographical position* (Hengl et al., [2018](#ref-hengl2018random)).

#### EML using the landmap package

Ensemble Machine Learning (EML) based on stacking is implemented in the
mlr package (Bischl et al., [2016](#ref-bischl2016mlr)) and can be
initiated via the function e.g.:

    m = makeStackedLearner(base.learners = lrns, super.learner = "regr.ml", method = "stack.cv")

here the base learner predictions will be computed by 5-fold
cross-validation (repeated re-fitting) and then used to determine the
meta-learner. This algorithm is known as the *SuperLearner* algorithm
(Polley & van der Laan, [2010](#ref-Polley2010)).

In the case of spatial prediction, we want to *block* training points
based on spatial proximity to prevent from producing bias predictions.
For this we should know the range of spatial dependence or similar
i.e. something that can be derived by fitting a variogram, then limit
the minimum spatial distance between training and validation points to
avoid overfitting or similar.

To automate fitting of an Ensemble Machine Learning models for the
purpose of spatial interpolation / prediction, one can now use the
[landmap](https://github.com/Envirometrix/landmap) package that
combines:

-   derivation of geographical distances,
-   conversion of grids to principal components,
-   automated filling of gaps in gridded data,
-   automated fitting of variogram and determination of spatial
    auto-correlation structure,
-   spatial overlay,
-   model training using spatial Cross-Validation (Lovelace, Nowosad, &
    Muenchow, [2019](#ref-lovelace2019geocomputation)),
-   model stacking i.e. fitting of the final EML,

The concept of automating spatial interpolation until the level that
almost no human interaction is required is referred to as **automated
mapping** or automated spatial interpolation (Pebesma et al.,
[2011](#ref-pebesma2011intamap)).

To install the most recent landmap package from Github use:

    library(devtools)
    install_github("envirometrix/landmap")

![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction of Zinc meuse data set
-----------------------------------------------------------------------------------------

#### Interpolation of numeric values using spatial regression

We load the packages that will be used in this tutorial:

    library(landmap)
    library(rgdal)
    library(geoR)
    library(plotKML)
    library(raster)
    library(glmnet)
    library(xgboost)
    library(kernlab)
    library(deepnet)
    library(mlr)

For testing we use meuse data set. We can fit a 2D model to interpolate
zinc concentration based on sampling points, distance to the river and
flooding frequency maps by using:

    demo(meuse, echo=FALSE)
    m <- train.spLearner(meuse["zinc"], covariates=meuse.grid[,c("dist","ffreq")], 
                         lambda = 1, parallel=FALSE)

    ## Converting ffreq to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Fitting a variogram using 'linkfit' and trend model...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: regr.ranger, regr.xgboost, regr.nnet, regr.ksvm, regr.cvglmnet...TRUE

    ## Fitting a spatial learner using 'mlr::makeRegrTask'...TRUE

    ## # weights:  103
    ## initial  value 51086662.343153 
    ## final  value 19148939.392857 
    ## converged
    ## # weights:  103
    ## initial  value 51785218.862406 
    ## final  value 19766323.697842 
    ## converged
    ## # weights:  103
    ## initial  value 50437020.956766 
    ## final  value 18581196.431655 
    ## converged
    ## # weights:  103
    ## initial  value 49411887.895259 
    ## final  value 18746103.885714 
    ## converged
    ## # weights:  103
    ## initial  value 48771807.220251 
    ## final  value 18285475.194245 
    ## converged
    ## # weights:  103
    ## initial  value 48499228.490663 
    ## final  value 18710243.571429 
    ## converged
    ## # weights:  103
    ## initial  value 48034737.310809 
    ## final  value 18502040.489209 
    ## converged
    ## # weights:  103
    ## initial  value 51804453.465291 
    ## final  value 19418420.345324 
    ## converged
    ## # weights:  103
    ## initial  value 48279721.515615 
    ## final  value 17748995.542857 
    ## converged
    ## # weights:  103
    ## initial  value 46875130.510756 
    ## final  value 17737086.935714 
    ## converged
    ## # weights:  103
    ## initial  value 54897384.436659 
    ## final  value 20750447.509677 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

This runs number of steps including derivation of geographical distances
(Møller, Beucher, Pouladi, & Greve, [2020](#ref-moller2020oblique)),
derivation of principal components (to make sure all features are
numeric and complete), fitting of variogram using the geoR package
(Diggle, Tawn, & Moyeed, [1998](#ref-diggle1998model)), spatial overlay,
training of individual learners and training of the super learner. The
only parameter we need to set manually in the `train.spLearner` is the
`lambda = 1` which is required to estimate variogram: in this case the
target variable is log-normally distributed, and hence the geoR package
needs the transformation parameter set at `lambda = 1`.

Note that the meta-learner is by default a linear model from five
independently fitted learners
`c("regr.ranger", "regr.xgboost", "regr.ksvm", "regr.nnet", "regr.cvglmnet")`.
We can check the success of training based on the 5-fold spatial
Cross-Validation using:

    summary(m@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -454.56 -101.76  -35.65   54.48 1145.80 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   600.00562  957.43482   0.627 0.531828    
    ## regr.ranger     0.78766    0.20742   3.797 0.000212 ***
    ## regr.xgboost   -0.14874    0.41545  -0.358 0.720836    
    ## regr.nnet      -1.30301    2.02394  -0.644 0.520695    
    ## regr.ksvm       0.41270    0.22706   1.818 0.071131 .  
    ## regr.cvglmnet  -0.08361    0.18738  -0.446 0.656117    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 215.3 on 149 degrees of freedom
    ## Multiple R-squared:  0.6671, Adjusted R-squared:  0.6559 
    ## F-statistic: 59.72 on 5 and 149 DF,  p-value: < 2.2e-16

Which shows that the model explains about 65% of variability in target
variable and that `regr.ranger` learner (Wright & Ziegler,
[2016](#ref-Wright2016)) is the strongest learner. Average mapping error
RMSE = 213.

To predict values at all grids we use:

    meuse.y <- predict(m)

    ## Predicting values using 'getStackedBaseLearnerPredictions'...TRUE

    ## Deriving model errors using ranger package 'quantreg' option...TRUE

Note that, by default, we will predict two outputs:

-   Mean prediction: i.e. the best unbiased prediction of response,
-   Prediction errors: usually predicted as lower and upper 67%
    quantiles based on the quantreg Random Forest algorithm
    (Meinshausen, [2006](#ref-meinshausen2006quantile)), or as a
    standard deviation of the bootstraped models,

To determine the prediction errors, we basically fit an independent
random forest model using the five base-learners with setting
`quantreg = TRUE`:

    zinc ~ regr.ranger + regr.xgboost + regr.nnet + regr.ksvm + regr.cvglmnet

The Quantreg technique (Meinshausen,
[2006](#ref-meinshausen2006quantile)) is a non-parameteric method for
determining prediction intervals and users can choose any probability in
the output via the `quantiles` argument. It can become, however,
computational for large number of features and trees in the random
forest, so have in mind that EML comes with exponentially increased
computing time.

We can plot the predictions and prediction errors next to each other by
using:

    par(mfrow=c(1,2), oma=c(0,0,0,1), mar=c(0,0,4,3))
    plot(raster(meuse.y$pred["response"]), col=R_pal[["rainbow_75"]][4:20],
      main="Predictions spLearner", axes=FALSE, box=FALSE)
      points(meuse, pch="+")
    plot(raster(meuse.y$pred["model.error"]), col=rev(bpy.colors()),
      main="Prediction errors", axes=FALSE, box=FALSE)
      points(meuse, pch="+")

<div class="figure">

<img src="README_files/figure-gfm/map-zinc-1.png" alt="Predicted zinc content based on meuse data set." width="70%" />
<p class="caption">
Predicted zinc content based on meuse data set.
</p>

</div>

This shows that the prediction errors (right plot) are the highest:

-   where the model is getting further away from the training points
    (spatial extrapolation),
-   where individual points with high values can not be explained by
    covariates,
-   where measured values of the response variable are in general high,

#### Model fine-tuning and feature selection

The function `tune.spLearner` can be used to further optimize spLearner
by:

-   fine-tuning model parameters, especially the ranger `mtry` and
    XGBoost parameters,
-   reduce number of features by running feature selection via the
    `mlr::makeFeatSelWrapper` function,

The package landmap currently requires that two base learners used
include `regr.ranger` and `regr.xgboost`, and that there are at least 3
base learners in total. The model from above can be optimized using:

    m0 <- tune.spLearner(m, xg.skip = TRUE, parallel=FALSE)

    ## Running tuneParams for ranger... TRUE

    ## [Tune] Started tuning learner regr.ranger for parameter set:

    ##          Type len Def                     Constr Req Tunable Trafo
    ## mtry discrete   -   - 2,5,8,11,14,16,19,22,25,28   -    TRUE     -

    ## With control class: TuneControlGrid

    ## Imputation value: Inf

    ## [Tune-x] 1: mtry=2

    ## [Tune-y] 1: mse.test.mean=49327.2957180; time: 0.0 min

    ## [Tune-x] 2: mtry=5

    ## [Tune-y] 2: mse.test.mean=50778.4474663; time: 0.0 min

    ## [Tune-x] 3: mtry=8

    ## [Tune-y] 3: mse.test.mean=51984.2350699; time: 0.0 min

    ## [Tune-x] 4: mtry=11

    ## [Tune-y] 4: mse.test.mean=50663.7549355; time: 0.0 min

    ## [Tune-x] 5: mtry=14

    ## [Tune-y] 5: mse.test.mean=54212.1779056; time: 0.0 min

    ## [Tune-x] 6: mtry=16

    ## [Tune-y] 6: mse.test.mean=56581.2132002; time: 0.0 min

    ## [Tune-x] 7: mtry=19

    ## [Tune-y] 7: mse.test.mean=51706.4517206; time: 0.0 min

    ## [Tune-x] 8: mtry=22

    ## [Tune-y] 8: mse.test.mean=51158.0016791; time: 0.0 min

    ## [Tune-x] 9: mtry=25

    ## [Tune-y] 9: mse.test.mean=52539.7490750; time: 0.0 min

    ## [Tune-x] 10: mtry=28

    ## [Tune-y] 10: mse.test.mean=54748.5281138; time: 0.0 min

    ## [Tune] Result: mtry=2 : mse.test.mean=49327.2957180

    ## [FeatSel] Started selecting features for learner 'regr.ranger'

    ## With control class: FeatSelControlRandom

    ## Imputation value: Inf

    ## [FeatSel-x] 1: 10111110111001101001001111000001 (18 bits)

    ## [FeatSel-y] 1: mse.test.mean=77276.5767830; time: 0.0 min

    ## [FeatSel-x] 1: 00100111101011011010001000110011 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=86054.5365033; time: 0.0 min

    ## [FeatSel-x] 1: 10011011110001111011001101111100 (20 bits)

    ## [FeatSel-y] 1: mse.test.mean=80682.8485265; time: 0.0 min

    ## [FeatSel-x] 1: 00001111111100000000011011000110 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=89788.8829711; time: 0.0 min

    ## [FeatSel-x] 1: 10100101101101000110100110001101 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=78766.4753449; time: 0.0 min

    ## [FeatSel-x] 1: 11000100111001000000100101111100 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=76057.6755581; time: 0.0 min

    ## [FeatSel-x] 1: 01011100000010110000111110001110 (15 bits)

    ## [FeatSel-y] 1: mse.test.mean=79104.5452494; time: 0.0 min

    ## [FeatSel-x] 1: 10101100000000110101100000110010 (12 bits)

    ## [FeatSel-y] 1: mse.test.mean=86088.3132842; time: 0.0 min

    ## [FeatSel-x] 1: 11001010001000010100101010011011 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=75522.0387556; time: 0.0 min

    ## [FeatSel-x] 1: 11010111110110010101001111001110 (20 bits)

    ## [FeatSel-y] 1: mse.test.mean=74440.3084668; time: 0.0 min

    ## [FeatSel-x] 1: 01101111011111111011101110111111 (26 bits)

    ## [FeatSel-y] 1: mse.test.mean=81025.0546582; time: 0.0 min

    ## [FeatSel-x] 1: 00101011111000100100001000100000 (11 bits)

    ## [FeatSel-y] 1: mse.test.mean=94101.4784305; time: 0.0 min

    ## [FeatSel-x] 1: 00010011110111000111010001110010 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=88236.3306223; time: 0.0 min

    ## [FeatSel-x] 1: 11011010110101010111000100111101 (19 bits)

    ## [FeatSel-y] 1: mse.test.mean=77855.3179068; time: 0.0 min

    ## [FeatSel-x] 1: 10101000110111111010100000110000 (15 bits)

    ## [FeatSel-y] 1: mse.test.mean=81236.4203691; time: 0.0 min

    ## [FeatSel-x] 1: 00101101001000001100011101010011 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=87357.0744277; time: 0.0 min

    ## [FeatSel-x] 1: 11001011000101110110000110010110 (16 bits)

    ## [FeatSel-y] 1: mse.test.mean=77135.7792684; time: 0.0 min

    ## [FeatSel-x] 1: 11000001011101101101100111000101 (17 bits)

    ## [FeatSel-y] 1: mse.test.mean=77972.9884845; time: 0.0 min

    ## [FeatSel-x] 1: 01101111110010000110000101011000 (15 bits)

    ## [FeatSel-y] 1: mse.test.mean=81152.7033291; time: 0.0 min

    ## [FeatSel-x] 1: 00111000001101100000001011011110 (14 bits)

    ## [FeatSel-y] 1: mse.test.mean=82684.5832381; time: 0.0 min

    ## [FeatSel] Result: PC1,PC2,PC4,rY_0,rX_0.2,rY_... (20 bits)

    ## Fiting Stacked Ensemble Model... TRUE

    ## # weights:  67
    ## initial  value 47786656.691319 
    ## final  value 18659856.827586 
    ## converged
    ## # weights:  67
    ## initial  value 53743348.703158 
    ## final  value 20151978.937063 
    ## converged
    ## # weights:  67
    ## initial  value 51436299.414363 
    ## final  value 19273314.000000 
    ## converged
    ## # weights:  67
    ## initial  value 51083183.129261 
    ## final  value 19942760.650000 
    ## converged
    ## # weights:  67
    ## initial  value 48550649.479630 
    ## final  value 17641584.027397 
    ## converged
    ## # weights:  67
    ## initial  value 48112914.674709 
    ## final  value 18376947.971014 
    ## converged
    ## # weights:  67
    ## initial  value 51166042.148834 
    ## final  value 18598419.723077 
    ## converged
    ## # weights:  67
    ## initial  value 41695825.804698 
    ## final  value 14524694.895522 
    ## converged
    ## # weights:  67
    ## initial  value 48151068.698759 
    ## final  value 19359367.625899 
    ## converged
    ## # weights:  67
    ## initial  value 52826598.932924 
    ## final  value 19832158.818841 
    ## converged
    ## # weights:  67
    ## initial  value 54860719.074523 
    ## final  value 20750447.509677 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

which reports RMSE for different `mtry` and reports which features have
been left and which removed. Note that we turn off the fine-tuning of
XGboost using `xg.skip = TRUE` as it takes at the order of magnitude
more time. In summary, in this specific case, the fine-tuned model is
not much more accurate, but it comes with the less features:

    str(m0@spModel$features)

    ##  chr [1:20] "PC1" "PC2" "PC4" "rY_0" "rX_0.2" "rY_0.2" "rX_0.5" "rY_0.5" "rY_0.7" "rX_1" "rY_1.2" "rY_1.4" "rY_1.7" ...

    summary(m0@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -472.83 -124.86  -54.02   75.15 1240.23 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   2102.9569   525.9619   3.998 0.000100 ***
    ## regr.ranger      0.3344     0.2314   1.445 0.150485    
    ## regr.xgboost     1.1964     0.4306   2.778 0.006170 ** 
    ## regr.nnet       -4.3036     1.0818  -3.978 0.000108 ***
    ## regr.ksvm        0.4092     0.2216   1.847 0.066802 .  
    ## regr.cvglmnet   -0.2138     0.1507  -1.419 0.158133    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 233.4 on 149 degrees of freedom
    ## Multiple R-squared:  0.6089, Adjusted R-squared:  0.5958 
    ## F-statistic:  46.4 on 5 and 149 DF,  p-value: < 2.2e-16

Fine-tuning and feature selection can be quite computational and it is
highly recommended to start with smaller subsets of data and then
measure processing time. Note that the function
`mlr::makeFeatSelWrapper` can result in errors if the covariates have a
low variance or follow a zero-inflated distribution. Reducing the number
of features via feature selection and fine-tuning of the Random Forest
`mtry` and XGboost parameters, however, can result in significantly
higher prediction speed and can also help improve accuracy.

#### Estimation of [prediction intervals](http://www.sthda.com/english/articles/40-regression-analysis/166-predict-in-r-model-predictions-and-confidence-intervals/)

We can also print the lower and upper [prediction
interval](http://www.sthda.com/english/articles/40-regression-analysis/166-predict-in-r-model-predictions-and-confidence-intervals/)
for every location using e.g.:

    sp::over(meuse[1,], meuse.y$pred)

    ##   response model.error   q.lwr q.upr
    ## 1 991.4288     204.118 752.764  1161

where `q.lwr` is the lower and `q.upr` is the 68% probability upper
quantile value. This shows that the 68% probability interval for the
location `x=181072, y=333611` is about 964–1161 which means that the
prediction error (±1 s.d.), at that location, is about 100. Compare with
the actual value sampled at that location:

    meuse@data[1,"zinc"]

    ## [1] 1022

The difference between predicted and observed values is much smaller
than estimated prediction error, but this is because this is the
training location hence the observed and predicted values match very
well.

The average prediction error for the whole area is:

    summary(meuse.y$pred$model.error)

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    7.00   35.93   71.00  111.53  179.50  466.00

which is somewhat lower than the RMSE derived by cross-validation, but
this is also because most of the predicted values are in fact low
(log-normal distribution), and EML seems not have many problems
predicting low values.

Note also, from the example above, if we refit a model using exactly the
same settings we might get somewhat different maps and different values.
This is to be expected as the number of training points and covariates
is low, the stacking is done by using (random) 5-fold Cross-validation,
and hence results will always be slightly different. The resulting
models and maps, however, should not be significantly different as this
would indicate that the EML is *unstable*.

#### Predictions using log-transformed target variable

If the purpose of spatial prediction to make a more accurate predictions
of low values of the response, then we can train a model with the
transformed variable:

    meuse$log.zinc = log1p(meuse$zinc)
    m2 <- train.spLearner(meuse["log.zinc"], covariates=meuse.grid[,c("dist","ffreq")], parallel=FALSE)

    ## Converting ffreq to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Fitting a variogram using 'linkfit' and trend model...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: regr.ranger, regr.xgboost, regr.nnet, regr.ksvm, regr.cvglmnet...TRUE

    ## Fitting a spatial learner using 'mlr::makeRegrTask'...TRUE

    ## # weights:  103
    ## initial  value 7287.917994 
    ## final  value 72.687798 
    ## converged
    ## # weights:  103
    ## initial  value 5600.806423 
    ## final  value 69.151006 
    ## converged
    ## # weights:  103
    ## initial  value 6085.461322 
    ## final  value 70.836359 
    ## converged
    ## # weights:  103
    ## initial  value 4487.894850 
    ## final  value 74.231879 
    ## converged
    ## # weights:  103
    ## initial  value 6211.876414 
    ## final  value 69.789404 
    ## converged
    ## # weights:  103
    ## initial  value 4179.569958 
    ## final  value 74.097931 
    ## converged
    ## # weights:  103
    ## initial  value 6295.856510 
    ## final  value 73.541035 
    ## converged
    ## # weights:  103
    ## initial  value 7126.267520 
    ## final  value 72.239539 
    ## converged
    ## # weights:  103
    ## initial  value 3778.407832 
    ## final  value 68.582899 
    ## converged
    ## # weights:  103
    ## initial  value 5399.074042 
    ## final  value 72.330991 
    ## converged
    ## # weights:  103
    ## initial  value 5378.577727 
    ## final  value 79.790191 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

The summary model will usually have a somewhat higher R-square, but the
best learners should stay about the same:

    summary(m2@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.92768 -0.20295 -0.03854  0.15896  1.38335 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)   
    ## (Intercept)   20.61908    8.33831   2.473   0.0145 * 
    ## regr.ranger    0.63380    0.21800   2.907   0.0042 **
    ## regr.xgboost   0.06163    0.33252   0.185   0.8532   
    ## regr.nnet     -3.58184    1.40735  -2.545   0.0119 * 
    ## regr.ksvm      0.33323    0.21179   1.573   0.1177   
    ## regr.cvglmnet  0.09531    0.15889   0.600   0.5495   
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.3626 on 149 degrees of freedom
    ## Multiple R-squared:  0.7545, Adjusted R-squared:  0.7463 
    ## F-statistic:  91.6 on 5 and 149 DF,  p-value: < 2.2e-16

We can next predict and then back-transform the values:

    meuse.y2 <- predict(m2)

    ## Predicting values using 'getStackedBaseLearnerPredictions'...TRUE

    ## Deriving model errors using ranger package 'quantreg' option...TRUE

    ## back-transform:
    meuse.y2$pred$response.t = expm1(meuse.y2$pred$response)

<div class="figure">

<img src="README_files/figure-gfm/map-zinc2-1.png" alt="Predicted zinc content based on meuse data set after log-transformation." width="70%" />
<p class="caption">
Predicted zinc content based on meuse data set after log-transformation.
</p>

</div>

If we compare distributions of two predictions (without and with
transformation) we can see that the predictions do not differ that much:

    library(ggridges)
    library(viridis)
    library(ggplot2)
    zinc.df = data.frame(zinc=c(sp::over(meuse, meuse.y$pred["response"])[,1], 
                                sp::over(meuse, meuse.y2$pred["response.t"])[,1],
                                meuse$zinc
                                ))
    zinc.df$type = as.vector(sapply(c("predicted", "log.predicted", "observed"), function(i){rep(i, nrow(meuse))}))
    ggplot(zinc.df, aes(x = zinc, y = type, fill = ..x..)) +
          geom_density_ridges_gradient(scale = 0.95, rel_min_height = 0.01, gradient_lwd = 1.) +
          scale_x_continuous(expand = c(0.01, 0)) +
          ## scale_x_continuous(trans='log2') +
          scale_y_discrete(expand = c(0.01, 0.01)) +
          scale_fill_viridis(name = "Zinc", option = "C") +
          labs(title = "Distributions comparison") +
      theme_ridges(font_size = 13, grid = TRUE) + theme(axis.title.y = element_blank())

    ## Picking joint bandwidth of 108

<div class="figure">

<img src="README_files/figure-gfm/hist-zinc2-1.png" alt="Difference in distributions observed and predicted." width="80%" />
<p class="caption">
Difference in distributions observed and predicted.
</p>

</div>

The observed very high values are somewhat smoothed out but the median
value is about the same, hence we can conclude that the two EML models
predict the target variable without a bias. To estimate the prediction
intervals using the log-transformed variable we can use:

    x = sp::over(meuse[1,], meuse.y2$pred)
    expm1(x$q.lwr); expm1(x$q.upr)

    ## [1] 765

    ## [1] 1190

In this case the prediction interval is somewhat wider than without
transformation.

Note that the log-transformation is not needed for a non-linear learner
such ranger and/or Xgboost, but it is often a good idea if the focus of
prediction is to get a better accuracy for lower values. For example, if
the objective of spatial interpolation is to map soil nutrient
deficiencies, then log-transformation is a good idea as it will produce
slightly better accuracy for lower values.

Another advantage of using log-transformation for log-normal variables
is that the prediction intervals would most likely be symmetric, so that
derivation of prediction error (±1 s.d.) can be derived by:

    pe = (q.upr - q.lwr)/2

![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction of soil types (factor-variable)
--------------------------------------------------------------------------------------------------

#### EML for classification

Ensemble Machine Learning can also be used to interpolate factor type
variables e.g. soil types. This is an example with the Ebergotzen
dataset available from the package plotKML (Hengl, Roudier, Beaudette,
Pebesma, & others, [2015](#ref-hengl2015plotkml)):

    library(plotKML)
    data(eberg_grid)
    gridded(eberg_grid) <- ~x+y
    proj4string(eberg_grid) <- CRS("+init=epsg:31467")
    data(eberg)
    coordinates(eberg) <- ~X+Y
    proj4string(eberg) <- CRS("+init=epsg:31467")
    summary(eberg$TAXGRSC)

    ##     Auenboden     Braunerde          Gley         HMoor    Kolluvisol          Moor Parabraunerde  Pararendzina 
    ##            71           790            86             1           186             1           704           215 
    ##       Pelosol    Pseudogley        Ranker       Regosol      Rendzina          NA's 
    ##           252           487            20           376            23           458

In this case the target variable is `TAXGRSC` soil types based on the
German soil classification system. This changes the modeling problem
from regression to classification. We recommend using the following
learners here:

    sl.c <- c("classif.ranger", "classif.xgboost", "classif.nnTrain")

The model training and prediction however looks the same as for the
regression:

    X <- eberg_grid[c("PRMGEO6","DEMSRT6","TWISRT6","TIRAST6")]
    mF <- train.spLearner(eberg["TAXGRSC"], covariates=X, parallel=FALSE)

    ## Converting PRMGEO6 to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Subsetting observations to 79% complete cases...TRUE

    ## Skipping variogram modeling...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: classif.ranger, classif.xgboost, classif.nnTrain...TRUE

    ## Fitting a spatial learner using 'mlr::makeClassifTask'...TRUE

To generate predictions we use:

    if(!exists("TAXGRSC")){
      TAXGRSC <- predict(mF)
    }

#### Classification accuracy

By default landmap package will predict both hard classes and
probabilities per class. We can check the average accuracy of
classification by using:

    newdata = mF@vgmModel$observations@data
    sel.e = complete.cases(newdata[,mF@spModel$features])
    newdata = newdata[sel.e, mF@spModel$features]
    pred = predict(mF@spModel, newdata=newdata)
    pred$data$truth = mF@vgmModel$observations@data[sel.e, "TAXGRSC"]
    print(calculateConfusionMatrix(pred))

    ##                predicted
    ## true            Auenboden Braunerde Gley Kolluvisol Parabraunerde Pararendzina Pelosol Pseudogley Ranker Regosol
    ##   Auenboden            34         5    1          0             4            0       0          4      0       0
    ##   Braunerde             0       617    1          0            19            7       8          9      0       7
    ##   Gley                  4         8   43          1             8            0       0          4      0       0
    ##   Kolluvisol            0        14    1         89            24            0       1          6      0       3
    ##   Parabraunerde         0        23    0          1           475            0       1         10      0       3
    ##   Pararendzina          0        23    0          0             3          143       5          2      0       0
    ##   Pelosol               0         9    0          1             1            3     157          5      0       1
    ##   Pseudogley            0        44    3          3            16            2       3        328      0      12
    ##   Ranker                0        10    0          0             6            0       1          0      0       0
    ##   Regosol               0        61    0          0             9            1       3         12      0     227
    ##   Rendzina              0         2    0          0             0            0       0          0      0       0
    ##   -err.-                4       199    6          6            90           13      22         52      0      26
    ##                predicted
    ## true            Rendzina -err.-
    ##   Auenboden            0     14
    ##   Braunerde            1     52
    ##   Gley                 0     25
    ##   Kolluvisol           0     49
    ##   Parabraunerde        0     38
    ##   Pararendzina         0     33
    ##   Pelosol              0     20
    ##   Pseudogley           0     83
    ##   Ranker               0     17
    ##   Regosol              0     86
    ##   Rendzina            20      2
    ##   -err.-               1    419

which shows that about 25% of classes are miss-classified and the
classification confusion is especially high for the `Braunerde` class.
Note the result above is based only on the internal training. Normally
one should repeat the process several times using 5-fold or similar (fit
EML, predict errors using resampled values only, repeat).

Predicted probabilities, however, are more interesting because they also
show where EML possibly has problems and which are the transition zones
between multiple classes:

<div class="figure">

<img src="README_files/figure-gfm/map-tax-1.png" alt="Predicted soil types based on EML." width="100%" />
<p class="caption">
Predicted soil types based on EML.
</p>

</div>

The maps show that also in this case geographical distances play a role,
but overall, the features (DTM derivatives and parnt material) seem to
be most important.

In addition to map of probabilities per class, we have also derived
errors per probability, which in this case can be computed as the
standard deviation between probabilities produced by individual learners
(for classification problems techniques such as quantreg random forest
do not exist):

<div class="figure">

<img src="README_files/figure-gfm/map-tax-error-1.png" alt="Predicted errors per soil types based on s.d. between individual learners." width="100%" />
<p class="caption">
Predicted errors per soil types based on s.d. between individual
learners.
</p>

</div>

In probability space, instead of using RMSE or similar measures, it is
often recommended to use the measures such as the
[log-loss](https://www.rdocumentation.org/packages/MLmetrics/versions/1.1.1/topics/LogLoss)
which correctly quantifies the difference between the observed and
predicted probability. As a rule of thumb, log-loss values above 0.35
indicate poor accuracy of predictions, but the threshold number for
critically low log-loss also depends on the number of classes. In the
plot above we can note that, in general, the average error in maps is
relatively low e.g. about 0.07:

    summary(TAXGRSC$pred$error.Parabraunerde)

    ##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    ## 0.005339 0.065914 0.078798 0.097867 0.098594 0.373669

but there are still many pixels where confusion between classes and
prediction errors are high.

![alt text](tex/R_logo.svg.png "Multi-scale") Example: Spatial prediction of soil organic carbon using 2-scale model
--------------------------------------------------------------------------------------------------------------------

In the next example we use EML to make spatial predictions using
data-set with 2 sets of covariates basically at different resolutions
250-m and 100-m. For this we use the Edgeroi data-set (Malone,
McBratney, Minasny, & Laslett, [2009](#ref-malone2009mapping)) used
commonly in the soil science to demonstrate 3D soil mapping of soil
organic carbon (g/kg) based on samples taken from diagnostic soil
horizons (multiple depth intervals):

    data(edgeroi)
    edgeroi.sp <- edgeroi$sites
    coordinates(edgeroi.sp) <- ~ LONGDA94 + LATGDA94
    proj4string(edgeroi.sp) <- CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs")
    edgeroi.sp <- spTransform(edgeroi.sp, CRS("+init=epsg:28355"))
    out.file = paste(getwd(), "output/edgeroi/edgeroi_training_points.gpkg", sep="/")
    #if(!file.exists("out.file")){
    #  writeOGR(edgeroi.sp, out.file, layer="edgeroi_training_points", driver = "GPKG")
    #}

We can fit two independent EML’s using the two sets of covariates and
then produce final predictions by combining them. We will refer to the
two models as coarse and fine-scale models. The fine-scale models will
often be much larger datasets and require serious computing capacity.

#### Coarse-scale model

First we use the 250-m resolution covariates:

    load("input/edgeroi.grids.rda")
    gridded(edgeroi.grids) <- ~x+y
    proj4string(edgeroi.grids) <- CRS("+init=epsg:28355")
    ov2 <- over(edgeroi.sp, edgeroi.grids)
    ov2$SOURCEID <- edgeroi.sp$SOURCEID
    ov2$x = edgeroi.sp@coords[,1]
    ov2$y = edgeroi.sp@coords[,2]

This is a 3D soil data set, so we also use the horizon `DEPTH` to
explain distribution of SOC in soil:

    source("PSM_functions.R")
    h2 <- hor2xyd(edgeroi$horizons)
    ## regression matrix:
    rm2 <- plyr::join_all(dfs = list(edgeroi$sites, h2, ov2))

    ## Joining by: SOURCEID
    ## Joining by: SOURCEID

    formulaStringP2 <- ORCDRC ~ DEMSRT5+TWISRT5+EV1MOD5+EV2MOD5+EV3MOD5+DEPTH
    rmP2 <- rm2[complete.cases(rm2[,all.vars(formulaStringP2)]),]
    str(rmP2[,all.vars(formulaStringP2)])

    ## 'data.frame':    4972 obs. of  7 variables:
    ##  $ ORCDRC : num  8.5 7.3 5 4.7 4.7 ...
    ##  $ DEMSRT5: num  198 198 198 198 198 198 185 185 185 185 ...
    ##  $ TWISRT5: num  19.5 19.5 19.5 19.5 19.5 19.5 19.2 19.2 19.2 19.2 ...
    ##  $ EV1MOD5: num  1.14 1.14 1.14 1.14 1.14 1.14 -4.7 -4.7 -4.7 -4.7 ...
    ##  $ EV2MOD5: num  1.62 1.62 1.62 1.62 1.62 1.62 3.46 3.46 3.46 3.46 ...
    ##  $ EV3MOD5: num  -5.74 -5.74 -5.74 -5.74 -5.74 -5.74 0.01 0.01 0.01 0.01 ...
    ##  $ DEPTH  : num  11.5 17.5 26 55 80 ...

We can now fit an EML directly by using the derived regression matrix:

    if(!exists("m.oc")){
      m.oc = train.spLearner.matrix(rmP2, formulaStringP2, edgeroi.grids, 
                            parallel=FALSE, cov.model="nugget", cell.size=1000)
    }

The geoR here reports problems as the data set is 3D and hence there are
spatial duplicates. We can ignore this problem and use the pre-defined
cell size of 1-km for spatial blocking, although in theory one can also
fit 3D variograms and then determine blocking parameter using training
data.

The results show that the EML model is significant:

    summary(m.oc@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -22.082  -1.035  -0.110   0.696  62.913 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   -0.29891    0.19619  -1.524   0.1277    
    ## regr.ranger    0.98144    0.02501  39.237  < 2e-16 ***
    ## regr.xgboost  -0.04209    0.07314  -0.576   0.5650    
    ## regr.nnet      0.05079    0.02841   1.788   0.0739 .  
    ## regr.ksvm      0.15289    0.02948   5.186 2.24e-07 ***
    ## regr.cvglmnet -0.09829    0.04113  -2.390   0.0169 *  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 3.152 on 4966 degrees of freedom
    ## Multiple R-squared:  0.6752, Adjusted R-squared:  0.6749 
    ## F-statistic:  2065 on 5 and 4966 DF,  p-value: < 2.2e-16

We can now predict values at e.g. 5-cm depth by adding a dummy spatial
layer with all fixed values:

    out.tif = "output/edgeroi/pred_oc_250m.tif"
    edgeroi.grids$DEPTH <- 5
    if(!exists("edgeroi.oc")){
      edgeroi.oc = predict(m.oc, edgeroi.grids[,m.oc@spModel$features])
    }
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.oc$pred["response"], out.tif, 
                options = c("COMPRESS=DEFLATE"))
      writeGDAL(edgeroi.oc$pred["response"], "output/edgeroi/pred_oc_250m_pe.tif", 
                options = c("COMPRESS=DEFLATE"))
    }

which shows the following:

<div class="figure">

<img src="README_files/figure-gfm/map-oc250m-1.png" alt="Predicted SOC content using 250-m covariates." width="100%" />
<p class="caption">
Predicted SOC content using 250-m covariates.
</p>

</div>

The average prediction error in the map is somewhat higher than the
average error from the model fitting:

    summary(edgeroi.oc$pred$model.error)

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##  0.5644  3.0055  5.0025  5.6059  7.8250 34.1500

This is because we are predicting the top-soil SOC, which is
exponentially higher at the soil surface and hence average model errors
for top soil should be slightly larger than the mean error for the whole
soil.

#### Fine-scale model

We can now fit the fine-scale model independently from the coarse-scale
model using the 100-m resolution covariates. In this case the 100-m
covariates are based on Landsat 8 and gamma radiometrics images (see
`?edgeroi` for more details):

    load("input/edgeroi.grids100.rda")
    gridded(edgeroi.grids100) <- ~x+y
    proj4string(edgeroi.grids100) <- CRS("+init=epsg:28355")
    ovF <- over(edgeroi.sp, edgeroi.grids100)
    ovF$SOURCEID <- edgeroi.sp$SOURCEID
    ovF$x = edgeroi.sp@coords[,1]
    ovF$y = edgeroi.sp@coords[,2]
    rmF <- plyr::join_all(dfs = list(edgeroi$sites, h2, ovF))

    ## Joining by: SOURCEID
    ## Joining by: SOURCEID

    formulaStringPF <- ORCDRC ~ MVBSRT6+TI1LAN6+TI2LAN6+PCKGAD6+RUTGAD6+PCTGAD6+DEPTH
    rmPF <- rmF[complete.cases(rmF[,all.vars(formulaStringPF)]),]
    str(rmPF[,all.vars(formulaStringPF)])

    ## 'data.frame':    5001 obs. of  8 variables:
    ##  $ ORCDRC : num  8.5 7.3 5 4.7 4.7 ...
    ##  $ MVBSRT6: num  5.97 5.97 5.97 5.97 5.97 5.97 6.7 6.7 6.7 6.7 ...
    ##  $ TI1LAN6: num  31.8 31.8 31.8 31.8 31.8 31.8 14.3 14.3 14.3 14.3 ...
    ##  $ TI2LAN6: num  32.9 32.9 32.9 32.9 32.9 32.9 22.1 22.1 22.1 22.1 ...
    ##  $ PCKGAD6: num  1.39 1.39 1.39 1.39 1.39 1.39 1.06 1.06 1.06 1.06 ...
    ##  $ RUTGAD6: num  0.14 0.14 0.14 0.14 0.14 0.14 0.16 0.16 0.16 0.16 ...
    ##  $ PCTGAD6: num  7.82 7.82 7.82 7.82 7.82 7.82 6.48 6.48 6.48 6.48 ...
    ##  $ DEPTH  : num  11.5 17.5 26 55 80 ...

We fit the 2nd fine-scale model:

    if(!exists("m.ocF")){
      m.ocF = train.spLearner.matrix(rmPF, formulaStringPF, edgeroi.grids100, 
                            parallel=FALSE, cov.model="nugget", cell.size=1000)
    }
    summary(m.ocF@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -20.414  -0.953  -0.033   0.710  61.120 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   -0.464404   0.119807  -3.876 0.000107 ***
    ## regr.ranger    1.158488   0.025040  46.265  < 2e-16 ***
    ## regr.xgboost   0.005880   0.073403   0.080 0.936151    
    ## regr.nnet      0.009628   0.022740   0.423 0.672020    
    ## regr.ksvm     -0.068891   0.029658  -2.323 0.020228 *  
    ## regr.cvglmnet -0.025746   0.029850  -0.863 0.388446    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 2.959 on 4995 degrees of freedom
    ## Multiple R-squared:  0.7163, Adjusted R-squared:  0.716 
    ## F-statistic:  2522 on 5 and 4995 DF,  p-value: < 2.2e-16

which shows that the 100-m resolution covariates help make even more
accurate predictions with R-square about 0.7. We can also make
predictions at 5-cm depth by using (note: this time it takes almost 6x
more time to compute predictions):

    edgeroi.grids100$DEPTH <- 5
    if(!exists("edgeroi.ocF")){
      edgeroi.ocF = predict(m.ocF, edgeroi.grids100[,m.ocF@spModel$features])
    }
    out.tif = "output/edgeroi/pred_oc_100m.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["response"], out.tif, options = c("COMPRESS=DEFLATE"))
      writeGDAL(edgeroi.ocF$pred["model.error"], "output/edgeroi/pred_oc_100m_pe.tif", options = c("COMPRESS=DEFLATE"))
    }

which shows the following:

<div class="figure">

<img src="README_files/figure-gfm/map-oc100m-1.png" alt="Predicted SOC content using 100-m covariates." width="100%" />
<p class="caption">
Predicted SOC content using 100-m covariates.
</p>

</div>

#### Merge multi-scale predictions

If we compare the coarse scale and fine scale predictions we see:

<img src="img/ebergotzen_two_scale_model_ensemble.gif" width="650" />

*Figure: Coarse-scale and fine-scale predictions of soil organic carbon
at 5-cm depth for the Edgeroi study area.*

Overall there is a match between general patterns but there are also
differences locally. This is to expect as the two models are fitted
independently using completely different covariates. We can merge the
two predictions and produce the final ensemble prediction by using the
following principles:

-   User prediction errors per pixel as weights so that more accurate
    predictions get higher weights,
-   Derive propagated error using the pooled variance based on
    individual predictions and errors,

Before we run this operation, we need to downscale the maps to the same
grid, best using Cubic-splines in GDAL:

    edgeroi.grids100@bbox

    ##       min     max
    ## x  741400  789000
    ## y 6646000 6678100

    outD.file = "output/edgeroi/pred_oc_250m_100m.tif"
    if(!file.exists(outD.file)){
      system(paste0('gdalwarp output/edgeroi/pred_oc_250m.tif ', outD.file,  
             ' -r \"cubicspline\" -te 741400 6646000 789000 6678100 -tr 100 100 -overwrite'))
      system(paste0('gdalwarp output/edgeroi/pred_oc_250m_pe.tif output/edgeroi/pred_oc_250m_100m_pe.tif',
             ' -r \"cubicspline\" -te 741400 6646000 789000 6678100 -tr 100 100 -overwrite'))
    }

We can now read the downscaled predictions, and merge them using the
prediction errors as weights (weighted average per pixel):

    edgeroi.ocF$pred$responseC = readGDAL("output/edgeroi/pred_oc_250m_100m.tif")$band1

    ## output/edgeroi/pred_oc_250m_100m.tif has GDAL driver GTiff 
    ## and has 321 rows and 476 columns

    edgeroi.ocF$pred$model.errorC = readGDAL("output/edgeroi/pred_oc_250m_100m_pe.tif")$band1

    ## output/edgeroi/pred_oc_250m_100m_pe.tif has GDAL driver GTiff 
    ## and has 321 rows and 476 columns

    X = comp.var(edgeroi.ocF$pred@data, r1="response", r2="responseC", v1="model.error", v2="model.errorC")
    edgeroi.ocF$pred$responseF = X$response
    out.tif = "output/edgeroi/pred_oc_100m_merged.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["responseF"], out.tif, options = c("COMPRESS=DEFLATE"))
    }

The final map of predictions is a combination of the two independently
produced predictions:

    plot(raster(edgeroi.ocF$pred["responseF"]), col=R_pal[["rainbow_75"]][4:20],
      main="Merged predictions spLearner", axes=FALSE, box=FALSE)
    points(edgeroi.sp, pch="+")

<div class="figure">

<img src="README_files/figure-gfm/map-2scale-1.png" alt="Merged predictions (coarse+fine scale) of SOC content at 100-m." width="80%" />
<p class="caption">
Merged predictions (coarse+fine scale) of SOC content at 100-m.
</p>

</div>

To merge the prediction errors, we use the pooled variance formula
(Rudmin, [2010](#ref-rudmin2010calculating)):

    comp.var

    ## function(x, r1, r2, v1, v2){
    ##   r =  rowSums( x[,c(r1, r2)] * 1/(x[,c(v1, v2)]^2) ) / rowSums( 1/(x[,c(v1, v2)]^2) )
    ##   ## https://stackoverflow.com/questions/13593196/standard-deviation-of-combined-data
    ##   v = sqrt( rowMeans(x[,c(r1, r2)]^2 + x[,c(v1, v2)]^2) - rowMeans( x[,c(r1, r2)])^2 )
    ##   return(data.frame(response=r, stdev=v))
    ## }
    ## <bytecode: 0x55b830c4c008>

    edgeroi.ocF$pred$model.errorF = X$stdev
    out.tif = "output/edgeroi/pred_oc_100m_merged_pe.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["model.errorF"], out.tif, options = c("COMPRESS=DEFLATE"))
    }

So in summary, merging multi-scale predictions is a straight forward
process, but it assumes that the reliable prediction errors are
available at both sides. The pooled variance might show higher errors
where predictions between independent models differ significantly and
this is correct. The 2-scale Ensemble Machine Learning method of
Predictive Soil Mapping was used for example to produce predictions of
[soil properties and nutrients of
Africa](https://www.isda-africa.com/isdasoil/).

Summary notes
-------------

The tutorial above demonstrates how to use Ensemble Machine Learning for
predictive mapping going from numeric 2D, to factor and to 3D variables.
Have in mind that the examples shown are based on relatively small
datasets, but can still become computational if you add even more
learners. In principle we do not recommend:

-   adding learners that are significantly less accurate than your best
    learners (i.e. focus on the top 4–5 best performing learners),
-   fitting EML for &lt;50–100 training points,
-   fitting EML for spatial interpolation where points are heavily
    spatially clustered,
-   using landmap package with large datasets,

You can also follow an introduction to Ensemble Machine Learning from
the [OpenGeoHub Summer School 2020 video
recordings](https://www.youtube.com/playlist?list=PLXUoTpMa_9s0Ea--KTV1OEvgxg-AMEOGv).

Please note that the mlr package is discontinued, so some of the example
above might become unstable with time. We are working on migrating the
code in the landmap package to make the `train.spLearner` function work
with the new [mlr3 package](https://mlr3.mlr-org.com/).

If you have a dataset that you have used to test Ensemble Machine
Learning, please come back to us and share your experiences by posting
[an issue](https://github.com/Envirometrix/landmap/issues) and/or
providing a screenshot of your results.

References
----------

<div id="refs" class="references hanging-indent">

<div id="ref-bischl2016mlr">

Bischl, B., Lang, M., Kotthoff, L., Schiffner, J., Richter, J.,
Studerus, E., … Jones, Z. M. (2016). mlr: Machine Learning in R. *The
Journal of Machine Learning Research*, *17*(1), 5938–5942.

</div>

<div id="ref-diggle1998model">

Diggle, P. J., Tawn, J. A., & Moyeed, R. A. (1998). Model-based
geostatistics. *Journal of the Royal Statistical Society: Series C
(Applied Statistics)*, *47*(3), 299–350.

</div>

<div id="ref-hengl2018random">

Hengl, T., Nussbaum, M., Wright, M. N., Heuvelink, G. B., & Gräler, B.
(2018). Random forest as a generic framework for predictive modeling of
spatial and spatio-temporal variables. *PeerJ*, *6*, e5518.

</div>

<div id="ref-hengl2015plotkml">

Hengl, T., Roudier, P., Beaudette, D., Pebesma, E., & others. (2015).
PlotKML: Scientific visualization of spatio-temporal data. *Journal of
Statistical Software*, *63*(5), 1–25.

</div>

<div id="ref-lovelace2019geocomputation">

Lovelace, R., Nowosad, J., & Muenchow, J. (2019). *Geocomputation with
r*. CRC Press.

</div>

<div id="ref-malone2009mapping">

Malone, B. P., McBratney, A., Minasny, B., & Laslett, G. (2009). Mapping
continuous depth functions of soil carbon storage and available water
capacity. *Geoderma*, *154*(1-2), 138–152.

</div>

<div id="ref-meinshausen2006quantile">

Meinshausen, N. (2006). Quantile regression forests. *Journal of Machine
Learning Research*, *7*(Jun), 983–999.

</div>

<div id="ref-moller2020oblique">

Møller, A. B., Beucher, A. M., Pouladi, N., & Greve, M. H. (2020).
Oblique geographic coordinates as covariates for digital soil mapping.
*SOIL*, *6*(2), 269–289.

</div>

<div id="ref-pebesma2011intamap">

Pebesma, E., Cornford, D., Dubois, G., Heuvelink, G. B., Hristopulos,
D., Pilz, J., … Skøien, J. O. (2011). INTAMAP: The design and
implementation of an interoperable automated interpolation web service.
*Computers & Geosciences*, *37*(3), 343–352.

</div>

<div id="ref-Polley2010">

Polley, E. C., & van der Laan, M. J. (2010). *Super learner in
prediction*. U.C. Berkeley Division of Biostatistics. Retrieved from
<https://biostats.bepress.com/ucbbiostat/paper266>

</div>

<div id="ref-rudmin2010calculating">

Rudmin, J. W. (2010). Calculating the exact pooled variance. *arXiv
Preprint*, *1007.1012*.

</div>

<div id="ref-seni2010ensemble">

Seni, G., & Elder, J. F. (2010). *Ensemble methods in data mining:
Improving accuracy through combining predictions*. Morgan & Claypool
Publishers.

</div>

<div id="ref-Wright2016">

Wright, M. N., & Ziegler, A. (2016). ranger: A Fast Implementation of
Random Forests for High Dimensional Data in C++ and R. *Journal of
Statistical Software*, (in press), 18.

</div>

<div id="ref-zhang2012ensemble">

Zhang, C., & Ma, Y. (2012). *Ensemble machine learning: Methods and
applications*. Springer New York.

</div>

</div>
